<?php



/**
 * Запускаем начальный метод send()
 */


class app
{

    /**
     * @param array $request $_REQUEST
     */

    static function init($request)
    {

        $controllerName = '\BX24\Ctrl\OSAGOBot';
        $methodName = 'send';

        $controller = new $controllerName($request);

        if (method_exists($controller, $methodName)) {

            $controller->$methodName();

        } else {

            trigger_error("Cant find method");

        }
    }

}