<?php


trait imBotTrait{

    /**
     * Получаем список регионов
     *
     * @param array $fields - массив полей, которые надо выбрать
     * @param string $filter - GET строка для фильтра. Начинаться должна с &
     *
     * @return array
     */

    public function getRegionList(array $fields, string $filter = ''){

        $result = [];

        $url = "http://geohelper.info/api/v1/regions?&apiKey=udY7yWi50yDbYqCm0R3Bhw1DWiyqmtUO&locale[lang]=ru&pagination[limit]=100" . $filter;

        $resgions = json_decode(file_get_contents($url), 1);

        foreach ($resgions['result'] as $key => $resgion) {
            foreach ($fields as $field) {
                $result[$key][$field] = $resgion[$field];
            }
        }

        return $result;

    }

    /**
     *
     *  !!!НЕ ИСПОЛЬЗУЕТСЯ!!!
     *
     * Проверяем существует ли такой город по названию
     *
     * @param string $cName Название города
     *
     * @return bool
     */

    public function getCity($cName): bool
    {
        $url = 'http://geohelper.info/api/v1/cities?&apiKey=' . GEO_API_KEY . '&locale[lang]=ru&filter[name]=' . $cName;
        $cities = json_decode(file_get_contents($url));
        $result = (count($cities->result) === 0) ? false : true;

        return $result;

    }

}