<?php

/**
 * Класс обрабатывающий событий для бота
 * ONAPPINSTALL - срабатываеть при установке бота в битрикс24
 * ONIMBOTJOINCHAT - срабатываеть при добавления бота в чат
 * ONIMBOTMESSAGEADD - срабатываеть при получении сообщения от пользователя
 */

namespace BX24\Ctrl;

class OSAGOBot extends ctrl
{

    public $installParams = [
        'CODE'                  => 'OSAGOBot',
        'TYPE'                  => 'O',
        'PROPERTIES'            =>
            [
                'NAME'              => 'БОТ ОСАГО',
                'LAST_NAME'         => '',
                'COLOR'             => 'GREEN',
                'EMAIL'             => 'madd.niko@gmail.com',
                'PERSONAL_BIRTHDAY' => '2018-01-01',
                'WORK_POSITION'     => 'Asnwer',
                'PERSONAL_WWW'      => '',
                'PERSONAL_GENDER'   => 'M',
                'PERSONAL_PHOTO'    => ''
            ]
    ];

    private $themeUserRelations = [
        ['id' => 1148, 'theme' => 'Покупка полиса'],
        ['id' => 512,  'theme' => 'Работоспособность систем'],
        ['id' => 3986, 'theme' => 'Расторжение договора'],
        ['id' => 1148, 'theme' => 'Консультация']

    ];


    public function __construct($request)
    {
        parent::__construct($request);
    }

    public function send()
    {

        switch ($this->request['event']){
            case 'ONAPPINSTALL':
                $this->model->installApp($this->installParams);
                break;
            case 'ONIMBOTJOINCHAT':
                $this->model->joinChat('Добрый день.');

                // Создает файл для подсчета шагов
                file_put_contents(TEMP . "/" . $this->request['data']['PARAMS']['DIALOG_ID'], "0");
                break;
            case 'ONIMCOMMANDADD':

                break;
            case 'ONIMBOTMESSAGEADD':

                $this->model->sendTyping($this->request['data']['PARAMS']['DIALOG_ID']);

                //Читает шаг и обновляет в файле
                $step = $this->model->checkStep($this->request['data']['PARAMS']['DIALOG_ID']);

                switch ($step){
                    case 1:
                        $this->model->sendBotMessage('Напишите, пожалуйста, свой номер телефона в формате [I]+79991234567[/I]');
                        break;
                    case 2:
                        $commandList = [];
                        $regions = $this->model->getRegionList(['id', 'name'], '&filter[countryIso]=ru');
                        foreach ($regions as $region) {
                            $commandList[] = ['command' => 'Регион проживания - ' . $region['name'], 'title' => $region['name']];
                        }
                        $commandOutput = $this->model->makeCommandList($commandList, '', '[BR]');
                        $this->model->sendBotMessage('Выберите регион проживания:[BR]' . $commandOutput);
                        break;
                    case 3:
                        $commandList =[];
                        $regions = $this->model->getRegionList(['id', 'name'], '&filter[countryIso]=ru');
                        foreach ($regions as $region) {
                            $commandList[] = ['command' => 'Регион использования - ' . $region['name'], 'title' => $region['name']];
                        }
                        $commandOutput = $this->model->makeCommandList($commandList, '', '[BR]');
                        $this->model->sendBotMessage('Где планируете эксплуатировать автомобиль:[BR]' . $commandOutput);
                        break;
                    case 4:
                        $commandList = [];
                        foreach ($this->themeUserRelations as $key => $themeUserRelation) {
                            $commandList[] = ['command' => $key, 'title' => $key . '. ' . $themeUserRelation['theme']];
                        }
                        $commandOutput = $this->model->makeCommandList($commandList, '', '[BR]');
                        $this->model->sendBotMessage('Выберите тему обращения:[BR]' . $commandOutput);
                        break;
                    case 5:
                        $this->model->sendBotMessage('Откуда вы узнали о компании Астро-Волга?');

                        $key = (int)$this->request['data']['PARAMS']['MESSAGE'];

                        //Если не передан пользователь, то трансфер идетна свободного оператора
                        if($key > 0){
                            $UID = $this->themeUserRelations[$key];
                            $this->model->redirectToOperator($this->request['data']['PARAMS']['CHAT_ID'], $UID);
                        }
                        else{
                            $this->model->redirectToFreeOperator($this->request['data']['PARAMS']['CHAT_ID']);
                            $this->model->leaveChat($this->request['data']['PARAMS']['CHAT_ID']);
                        }
                        break;
                    default:
                        unlink(TEMP . "/" . $this->request['data']['PARAMS']['DIALOG_ID']);
                        break;

                }
                break;


        }
    }

}