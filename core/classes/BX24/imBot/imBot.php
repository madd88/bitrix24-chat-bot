<?php

namespace BX24\imBot;

/**
 * @author  Nikolaev Aleksei
 * @version 1.0
 * @access  public
 * */
class imBot
{

    use \imBotTrait;

    public $request = [];

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Утановка бота в Битрикс24
     *
     * @param array $params
     *              [CODE => Код бота,
     *               TYPE => B - Бот, O - Бот открытых линий,
     *               PROPERTIES => [
     *                      NAME => Имя бота,
     *                      LAST_NAME => Фамилия бота(необязательно),
     *                      COLOR => Цвет бота,
     *                      EMAIL => Почта бота,
     *                      PERSONAL_BIRTHDAY => День рождения,
     *                      WORK_POSITION => Краткое опичание,
     *                      PERSONAL_WWW => Сайт,
     *                      PERSONAL_GENDER => Пол,
     *                      PERSONAL_PHOTO => Аватар,
     *                  ]
     *
     *              ]
     */

    public function installApp($params = [])
    {

        $backUrl = ($_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'] . (in_array($_SERVER['SERVER_PORT'],
                array(80, 443)) ? '' : ':' . $_SERVER['SERVER_PORT']) . $_SERVER['SCRIPT_NAME'];

        $this->sendRequest('imbot.register',
            [
                'CODE'                  => $params['CODE'],
                'TYPE'                  => $params['TYPE'],
                'EVENT_MESSAGE_ADD'     => $backUrl,
                'EVENT_WELCOME_MESSAGE' => $backUrl,
                'EVENT_BOT_DELETE'      => $backUrl,
                'PROPERTIES'            =>
                    [
                        'NAME'              => $params['PROPERTIES']['NAME'],
                        'LAST_NAME'         => $params['PROPERTIES']['LAST_NAME'],
                        'COLOR'             => $params['PROPERTIES']['COLOR'],
                        'EMAIL'             => $params['PROPERTIES']['EMAIL'],
                        'PERSONAL_BIRTHDAY' => $params['PROPERTIES']['PERSONAL_BIRTHDAY'],
                        'WORK_POSITION'     => $params['PROPERTIES']['WORK_POSITION'],
                        'PERSONAL_WWW'      => $params['PROPERTIES']['PERSONAL_WWW'],
                        'PERSONAL_GENDER'   => $params['PROPERTIES']['PERSONAL_GENDER'],
                        'PERSONAL_PHOTO'    => $params['PROPERTIES']['PERSONAL_PHOTO']
                    ]
            ],
            $this->request["auth"]);
    }

    /**
     * Выводит сообщение при соединении к чату
     *
     * @param string $message Приветственное сообщение
     *
     * @return void
     */

    public function joinChat(string $message = '')
    {
       $this->sendRequest('imbot.message.add', [
            'BOT_ID'    => $this->request['data']['PARAMS']['BOT_ID'],
            'DIALOG_ID' => $this->request['data']['PARAMS']['DIALOG_ID'],
            'MESSAGE'   => $message
        ], $this->request['auth']);

    }


    /**
     * Отправка запроса в Bitrix24
     *
     * @param string $method имя метода
     * @param array  $params список параметров запроса
     * @param array  $auth   bitrix24 массив аутентификации
     *
     * @return string
     */
    public function sendRequest(string $method, array $params = [], array $auth = [])
    {
        $URL = 'https://' . $auth['domain'] . '/rest/' . $method;
        $queryParams = http_build_query(array_merge($params, ['auth' => $auth['access_token']]));
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_POST           => 1,
            CURLOPT_HEADER         => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL            => $URL,
            CURLOPT_POSTFIELDS     => $queryParams,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result, 1);

        return $result;
    }


    /**
     *  Формирует комманду PUT для вывода в чат
     *
     * @param array $params - команда и ее заголовок
     *
     * @return string
     */

    public function makePUT(array $params = []) : string
    {

            return '[put=' . $params['command'] . ']' . $params['title'] . '[/put]';

    }

    /**
     *  Формирует список команд send для вывода в чат
     *
     * @param array $params - массив комманд.
     * @param string $delimiter - Разделенитель между командами.
     * @param string $delimiter - Перевод на новую строку, если надо - [BR] .
     *
     * @return string
     */

    public function makeCommandList(array $params = [], $delimiter = '', $newLine = '') : string
    {

        $result = '';

        foreach ($params as $param) {
            $result .= '[send=' . $param['command'] . ']' . $param['title'] . '[/send]' . $delimiter . $newLine;
        }

        return $result;
    }


    /**
     * Отправка ботом сообщения в чат
     *
     * @param string $message - Сообщение
     *
     * @return void
     * */

    public function sendBotMessage($message){
        $this->sendRequest('imbot.message.add', [
            'BOT_ID'    => $this->request['data']['PARAMS']['BOT_ID'],
            "DIALOG_ID" => $this->request['data']['PARAMS']['DIALOG_ID'],
            "MESSAGE"   => $message
        ], $this->request["auth"]);
    }

    /**
     * Подключение свободного оператора к чату
     *
     * @param int $chatID - ИД Чата в котором сидит бот
     *
     * @return void
     * */

    public function redirectToFreeOperator(int $chatID = 0)
    {
        $this->sendRequest('imopenlines.bot.session.operator', [
            'CHAT_ID'    => $chatID
        ], $this->request["auth"]);
    }

    /**
     * Подключение оператора по ID к чату
     *
     * @param int $chatID - ИД Чата в котором сидит бот
     * @param int $UID - ИД оператора
     * @param string $leave - Y/N - бот покидает чат или нет
     *
     * @return void
     * */

    public function redirectToOperator(int $chatID = 0, int $UID = 0, string $leave = 'Y')
    {
        $this->sendRequest('imopenlines.bot.session.operator', [
            'CHAT_ID' => $chatID,
            'USER_ID' => $UID,
            'LEAVE'   => $leave
        ], $this->request["auth"]);
    }

    /**
     * Отправка "Вам печатают..." в чат
     *
     * @param string $dialogID - ИД Диалога в котором сидит бот
     *
     * @return void
     * */

    public function sendTyping($dialogID){
        $this->sendRequest('imbot.chat.sendTyping', [
            'DIALOG_ID'    => $dialogID
        ], $this->request["auth"]);
    }

    /**
     * Получение следующего шага для бота
     *
     * @param string $dialogID - ИД Диалога в котором сидит бот
     *
     * @return int
     * */

    public function checkStep(string $dialogID) : int
    {

        $currentStep = (int)file_get_contents(TEMP . "/" . $dialogID);
        file_put_contents(TEMP . "/" . $dialogID, ++$currentStep);

        return $currentStep;

    }

    public function leaveChat($chatID = 0){
        $this->sendRequest('imbot.chat.leave', Array(
            'CHAT_ID' => $chatID
        ), $this->request["auth"]);
    }


}